package no.uib.info233.v2016.gep003_skn003.oblig3;

public class Bruker {
	
	private String brukernavn;
	private String email;
	private String fultNavn;
	private String ansettelsesDato;
	private String passord;
	
	/**
	 * setter passord
	 * @return passord
	 */
	public String getPassord() {
		return passord;
	}

	/**
	 * returnerer passord
	 * @param passord
	 */
	public void setPassord(String passord) {
		this.passord = passord;
	}

	@Override
	public String toString() {
		return "Bruker [brukernavn=" + brukernavn + ", email=" + email + ", fultNavn=" + fultNavn + ", ansettelsesDato="
				+ ansettelsesDato + ", passord=" + passord + "]";
	}

	/**
	 * returnerer brukernavn
	 * @return brukernavn
	 */
	public String getBrukernavn() {
		return brukernavn;
	}
	
	/**
	 * setter brukernavn
	 * @param brukernavn
	 */
	public void setBrukernavn(String brukernavn) {
		this.brukernavn = brukernavn;
	}
	
	/**
	 * returneren email
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * setter email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * returnerer fultNavn
	 * @return fultnavn
	 */
	public String getFultNavn() {
		return fultNavn;
	}
	
	/**
	 * setter fultNavn
	 * @param fultNavn
	 */
	public void setFultNavn(String fultNavn) {
		this.fultNavn = fultNavn;
	}
	
	/**
	 * returnerer ansettelsesDato
	 * @return ansettelsesDato
	 */
	public String getAnsettelsesDato() {
		return ansettelsesDato;
	}
	
	/**
	 * ansettelsesDato
	 * @param ansettelsesDato
	 */
	public void setAnsettelsesDato(String ansettelsesDato) {
		this.ansettelsesDato = ansettelsesDato;
	}
	
	/**
	 * KonstruktÝr for bruker
	 * @param brukernavn
	 * @param email
	 * @param fultNavn
	 * @param ansettelsesDato
	 */
	public Bruker(String brukernavn, String email, String fultNavn, String ansettelsesDato, String passord) {
		super();
		this.brukernavn = brukernavn;
		this.email = email;
		this.fultNavn = fultNavn;
		this.ansettelsesDato = ansettelsesDato;
		this.passord = passord;
	}

}
