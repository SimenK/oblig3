package no.uib.info233.v2016.gep003_skn003.oblig3.hovedProgram;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.net.ssl.SSLEngineResult.Status;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import no.uib.info233.v2016.gep003_skn003.oblig3.Bruker;

public class Logic {
	GUI GUIObjekt;
	JComboBox overskrifter;
	JComboBox sortertBox;

	JPanel redigeringsKnapper;
	JPanel sok;
	JTextField sokefelt;
	Button sokButton;
	Button redigerButton;
	Button nyButton;
	Button nyBrukerButton;
	Button visBrukereButton;

	JTable table;

	ArrayList<Issue> issueList;
	ArrayList<Bruker> users;

	JScrollPane scrollPane;

	JPanel editor;
	JLabel idLabel;
	JLabel userLabel;
	JLabel createdLabel;
	JTextArea textArea;
	JTextField priorityField;
	JTextField locationField;
	JComboBox userList;
	JComboBox priorityList;
	Button lagreButton;
	Button avbrytButton = new Button("Avbryt");

	JLabel brukernavnLabel;
	JTextField brukernavnField;
	JLabel emailLabel;
	JTextField emailField;
	JLabel fultNavnLabel;
	JTextField fultNavnField;
	JLabel ansettelsesDatoLabel;
	JTextField ansettelsesDatoField;
	JLabel passordLabel;
	JTextField passordField;
	Button nyAnsatt;

	Button nyAnsattButton;
	Button visIssuesButton;

	int maxId = 0;
	String currentUsername;
	String currentLoggedIn;

	boolean visLoste = false;
	boolean visIkkeEndret = false;

	JCheckBox visLostCheckbox;
	JCheckBox endretCheckBox;

	/**
	 * Oppretter et logic objekt og initialiserer Actionlistnere
	 * 
	 * @param objekt
	 *            et gui objekt
	 */
	Logic(GUI objekt) {
		readXML();
		GUIObjekt = objekt;
		GUIObjekt.getLoggInnButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean userFound = false;
				int i = 0;
				while (i < users.size() && !userFound) {
					if (users.get(i).getBrukernavn().equals(GUIObjekt.getBrukernavnField().getText()))
						userFound = true;
				}
				if (userFound && users.get(i).getPassord().equals(GUIObjekt.getPassordField().getText())) {
					currentUsername = GUIObjekt.getBrukernavnField().getText();
					currentLoggedIn = GUIObjekt.getBrukernavnField().getText();
					showList();
					GUIObjekt.getFrame().setVisible(false);
					redigerButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							editIssue((int) table.getValueAt(table.getSelectedRow(), 0),
									(String) table.getValueAt(table.getSelectedRow(), 1),
									(String) table.getValueAt(table.getSelectedRow(), 2),
									(String) table.getValueAt(table.getSelectedRow(), 3),
									(String) table.getValueAt(table.getSelectedRow(), 4),
									(String) table.getValueAt(table.getSelectedRow(), 5),
									(String) table.getValueAt(table.getSelectedRow(), 6));

						}
					});

					visBrukereButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							visBrukere();
							visIssuesButton.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									currentUsername = (String) table.getValueAt(table.getSelectedRow(), 0);
									showList();

								}
							});

						}
					});
					sokButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							if (overskrifter.getSelectedIndex() == 0) {
								sokDato(sokefelt.getText());
							} else {
								try {
									sokPrioritering(sokefelt.getText());
								} catch (NumberFormatException f) {
									return;
								}
							}

						}
					});

					nyBrukerButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							showList();

						}
					});
					avbrytButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							gaTilHovedVindu();
						}
					});
				}
			}
		});

	}

	/**
	 * Tar inn en liste og returnerer et Jtable
	 * 
	 * @param list
	 *            en ArrayList med issue objekter
	 * @return Jtable som inneholder elementene i Issue listen
	 */
	public JTable returnIssueTable(ArrayList<Issue> list) {
		Object rowData[][] = new Object[list.size()][9];
		int i = 0;
		int j = 0;
		for (Issue o : list) {
			j = 0;
			rowData[i][j++] = o.getId();
			rowData[i][j++] = o.getUser();
			rowData[i][j++] = o.getCreated();
			rowData[i][j++] = o.getText();
			rowData[i][j++] = o.getPriority();
			rowData[i][j++] = o.getLocation();
			rowData[i][j++] = o.getStatus();
			rowData[i][j++] = o.getOpprettetAv();
			rowData[i++][j] = o.getEndretAv();

		}
		Object columnNames[] = { "ID", "User", "Created", "Text", "Priority", "Location", "Status", "Opprettet av",
				"Sist endret av" };
		JTable table = new JTable(rowData, columnNames) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			};
		};
		return table;
	}

	public JTable returnBrukerTable(ArrayList<Bruker> list) {
		Object rowData[][] = new Object[list.size()][4];
		int i = 0;
		int j = 0;
		for (Bruker o : list) {
			j = 0;
			rowData[i][j++] = o.getBrukernavn();
			rowData[i][j++] = o.getFultNavn();
			rowData[i][j++] = o.getEmail();
			rowData[i++][j++] = o.getAnsettelsesDato();
		}
		Object columnNames[] = { "Brukernavn", "Fullt navn", "E-mail", "Ansettelses dato" };
		JTable table = new JTable(rowData, columnNames) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			};
		};
		return table;
	}

	/**
	 * Leser old_issues.xml og brukere.xml, omgj�r informasjonen til Issue
	 * objekter og Bruker objekter og putter de i arraylister.
	 * 
	 * @return issueList liste med Issues;
	 */
	public ArrayList<Issue> readXML() {
		issueList = new ArrayList<Issue>();
		users = new ArrayList<Bruker>();
		try {
			String linje;

			BufferedReader rdr = new BufferedReader(
					new InputStreamReader(new FileInputStream("new_issues.xml"), "UTF8"));
			rdr.readLine();
			int teller = 0;
			while ((linje = rdr.readLine()) != null) {
				String liste[] = linje.split("\"");
				if (liste.length > 5 && liste[0] != null) {
					issueList.add(new Issue(Integer.parseInt(liste[1]), liste[3], liste[5], liste[7], liste[9],
							liste[11], liste[13], liste[15], liste[17]));
					teller++;
					if (Integer.parseInt(liste[1]) > maxId)
						maxId = Integer.parseInt(liste[1]);
				}
			}
			System.out.println("Teller: " + teller + " nummer: " + issueList.size() + " maxId: " + maxId);
			rdr.close();
			rdr = new BufferedReader(new FileReader("brukere.xml"));
			rdr.readLine();
			while ((linje = rdr.readLine()) != null) {
				String liste[] = linje.split("\"");
				if (liste.length > 4 && liste[0] != null) {
					users.add(new Bruker(liste[1], liste[3], liste[5], liste[7], liste[9]));
				}
			}

			return issueList;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Alt gikk galt");
			return null;
		}
	}

	/**
	 * Getter for IssueList
	 * 
	 * @return issueList med alle issuene
	 */
	public ArrayList<Issue> getIssueList() {
		return issueList;
	}

	/**
	 * Getter for Users
	 * 
	 * @return Users med alle brukerene
	 */
	public ArrayList<Bruker> getUsers() {
		return users;
	}

	/**
	 * Getter for maxId
	 * 
	 * @return returnerer den h�yeste Iden funnet i et issue
	 */
	public int getMaxId() {
		return maxId;
	}

	/**
	 * Tar inn en ArrayList med issue objekter og et brukernavn. Den putter
	 * brukerens issue objekter i en liste og returnerer den.
	 * 
	 * @param liste
	 *            ArrayList med Issue objekter
	 * @param brukernavn
	 *            String til bruk i s�k i liste
	 * @return ArrayList<Issue> en liste med brukerens Issues
	 */
	private ArrayList<Issue> findUsersIssues(ArrayList<Issue> liste, String brukernavn) {
		ArrayList<Issue> userList = new ArrayList<Issue>();
		if (currentLoggedIn.equals(currentUsername)) {
			if (visLoste)
				for (Issue o : liste) {
					if (o.getUser().equals(brukernavn) || o.getOpprettetAv().equals(brukernavn)
							&& createDate(o.getCreated()).after(new Date(System.currentTimeMillis() - (604800000)))) {
						userList.add(o);
					}
				}
			else {
				for (Issue o : liste) {
					if (o.getUser().equals(brukernavn) && o.getStatus().equals("ul�st")
							|| o.getOpprettetAv().equals(brukernavn) && createDate(o.getCreated())
									.after(new Date(System.currentTimeMillis() - (604800000))))
						userList.add(o);
				}
			}
		} else {
			if (visLoste)
				for (Issue o : liste) {
					if (o.getUser().equals(brukernavn)) {
						userList.add(o);
					}
				}
			else {
				for (Issue o : liste) {
					if (o.getUser().equals(brukernavn) && o.getStatus().equals("ul�st"))
						userList.add(o);
				}
			}
		}
		if (visIkkeEndret) {
			ArrayList<Issue> tempList = new ArrayList<Issue>();
			for (Issue o : userList)
				if (o.getEndretAv().equals(""))
					tempList.add(o);
			userList.clear();
			userList.addAll(tempList);
		}

		return userList;

	}

	/**
	 * Initialiserer den f�rste JTable og knappene til hovedvinduet
	 * 
	 * @param pane
	 *            Tar inn GUIs pane
	 */
	private void showList() {
		showList(issueList);

	}

	private void showList(ArrayList<Issue> liste) {
		visLostCheckbox = new JCheckBox("Vis l�ste");
		visLostCheckbox.setSelected(visLoste);
		endretCheckBox = new JCheckBox("Har ikke blitt endret");
		endretCheckBox.setSelected(visIkkeEndret);

		String[] sortListe = { "Sorter etter", "Priority", "Dato", "Status" };
		sortertBox = new JComboBox(sortListe);

		visLostCheckbox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				visLoste = visLostCheckbox.isSelected();
				showList();
			}
		});
		endretCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				visIkkeEndret = endretCheckBox.isSelected();
				showList();
			}
		});
		sortertBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (sortertBox.getSelectedIndex()) {
				case 0:
					showList();
					break;
				case 1:
					showList(prioriterSort("ikke prioritert"));
					break;
				case 2:
					showList(sortDato(issueList));
					break;
				case 3:
					showList(sortLost(issueList));
					break;
				}
			}
		});
		JPanel pane = new JPanel();
		GUIObjekt.invalidate();
		GUIObjekt.validate();

		redigeringsKnapper = new JPanel();
		sok = new JPanel();
		redigeringsKnapper.setLayout(new GridLayout(1, 2));
		sok.setLayout(new GridLayout(1, 2));

		String[] overskiftsListe = { "Created", "Priority" };
		overskrifter = new JComboBox(overskiftsListe);

		sokefelt = new JTextField("");
		sokefelt.setPreferredSize(new Dimension(150, 100));
		sokButton = new Button("S�k");

		redigerButton = new Button("Rediger");
		nyButton = new Button("Ny");
		nyBrukerButton = new Button("Ny Bruker");
		visBrukereButton = new Button("Vis brukere");

		redigeringsKnapper.add(redigerButton);
		// redigeringsKnapper.add(nyButton);
		redigeringsKnapper.add(nyBrukerButton);
		redigeringsKnapper.add(visBrukereButton);

		sok.add(sortertBox);
		sok.add(overskrifter);
		sok.add(sokefelt);
		sok.add(sokButton);
		sok.add(visLostCheckbox);
		sok.add(endretCheckBox);

		table = returnIssueTable(findUsersIssues(liste, currentUsername));
		table.setAutoCreateRowSorter(true);
		pane.setLayout(new BorderLayout());
		scrollPane = new JScrollPane(table);
		pane.add(scrollPane, BorderLayout.NORTH);
		pane.add(redigeringsKnapper, BorderLayout.WEST);
		pane.add(sok, BorderLayout.EAST);

		GUIObjekt.setContentPane(pane);
		GUIObjekt.invalidate();
		GUIObjekt.validate();
	}

	/**
	 * Tar inn all informasjonen til et Issue s� den kan redigere informasjonen,
	 * hvis parameterene er tomme s� g�r metoden utifra at den skal opprette et
	 * nytt Issue heller enn � redigere
	 * 
	 * @param id
	 *            iden til et Issue
	 * @param user
	 *            brukeren til et Issue
	 * @param created
	 *            opprettelsesdatoen til et Issue
	 * @param text
	 *            teksten til et Issue
	 * @param priority
	 *            prioriteten til et Issue
	 * @param location
	 *            location til et Issue
	 */
	private void editIssue(int id, String user, String created, String text, String priority, String location,
			String status) {
		editor = new JPanel();
		boolean ny;
		textArea = new JTextArea(text);
		priorityField = new JTextField(priority);
		locationField = new JTextField(location);
		JLabel priorityLabel = new JLabel("Priority: ");
		JLabel locationLabel = new JLabel("Location: ");
		lagreButton = new Button("Lagre");

		if (created.equals(""))
			ny = true;

		else
			ny = false;
		int newId;
		if (ny)
			newId = issueList.get(issueList.size() - 1).getId() + 1;
		else
			newId = id;

		idLabel = new JLabel("ID: " + newId);
		editor.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		int prioritet;
		String[] priorityArray = new String[] { "ikke prioritert", "normal", "h�y", "kritisk" };
		switch (priority) {
		case "ikke prioritert":
			prioritet = 0;
			break;
		case "normal":
			prioritet = 1;
			break;
		case "h�y":
			prioritet = 2;
			break;
		case "kritisk":
			prioritet = 3;
			break;
		default:
			prioritet = 0;
		}
		priorityList = new JComboBox(priorityArray);
		priorityList.setSelectedIndex(prioritet);

		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;

		String[] usersArray = new String[users.size()];
		if (ny) {
			for (int i = 0; i < users.size(); i++)
				usersArray[i] = users.get(i).getBrukernavn();
			userList = new JComboBox(usersArray);
			editor.add(userList, c);
		} else {
			for (int i = 0; i < users.size(); i++)
				usersArray[i] = users.get(i).getBrukernavn();
			userList = new JComboBox(usersArray);
			userList.setSelectedItem(user);
			editor.add(userList, c);
		}
		editor.add(userLabel = new JLabel("User: " + user), c);
		String newCreated;
		if (ny) {
			Date date = new Date();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int year = localDate.getYear();
			int month = localDate.getMonthValue();
			int day = localDate.getDayOfMonth();
			newCreated = "" + month + "/" + day + "/" + year;
		} else
			newCreated = created;
		c.gridx = 1;
		c.gridy = 0;
		editor.add(createdLabel = new JLabel("Created: " + newCreated), c);

		c.gridx = 2;
		c.gridy = 0;
		editor.add(idLabel, c);
		JCheckBox solved = new JCheckBox("L�st");
		if (!ny) {
			editor.add(solved);

		}
		// editor.add(userLabel);
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 4;
		editor.add(textArea, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 2;
		editor.add(priorityLabel, c);
		c.gridx = 1;
		c.gridy = 2;
		editor.add(priorityList, c);
		c.gridx = 2;
		c.gridy = 2;
		editor.add(locationLabel, c);
		c.gridx = 3;
		c.gridy = 2;
		editor.add(locationField, c);
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 2;
		editor.add(lagreButton, c);
		c.gridx = 2;
		c.gridy = 3;
		editor.add(avbrytButton, c);
		GUIObjekt.setContentPane(editor);
		// table.setVisible(false);
		GUIObjekt.invalidate();
		GUIObjekt.validate();
		editor.setVisible(true);

		lagreButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textArea.getText().equals("") && !locationField.getText().equals("")) {
					if (!ny) {
						String lost;
						if (solved.isSelected()) {
							lost = "L�st";
						} else {
							lost = "Ul�st";
						}
						for (int i = 0; i < issueList.size(); i++) {
							if (issueList.get(i).getId() == id)
								issueList.set(i, new Issue(newId, (String) userList.getSelectedItem(), newCreated,
										textArea.getText(), (String) priorityList.getSelectedItem(), lost,
										issueList.get(i).getOpprettetAv(), currentLoggedIn, locationField.getText()));
						}
					} else
						issueList.add(new Issue(newId, (String) userList.getSelectedItem(), newCreated,
								textArea.getText(), (String) priorityList.getSelectedItem(), "Ul�st", currentLoggedIn,
								"", locationField.getText()));
					lagreIssuesTilFil();

					showList();
				} else {
					JOptionPane.showMessageDialog(null, "Du m� fylle ut tekst og location",
							"Du m� fylle ut tekst og location", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

	}

	/**
	 * Metode for � vise en liste med brukere, hvor man kan gjennom knappene f�
	 * se issuene til valgte brukere
	 */

	private void visBrukere() {
		visIssuesButton = new Button("Vis brukers issues");
		JPanel knapper = new JPanel();
		knapper.add(visIssuesButton);
		knapper.add(avbrytButton);

		table = returnBrukerTable(users);
		table.setAutoCreateRowSorter(true);
		JScrollPane brukerePane = new JScrollPane(table);
		// scrollPane = new JScrollPane(table);

		JPanel newJPanel = new JPanel();
		newJPanel.setLayout(new BorderLayout());
		newJPanel.add(brukerePane, BorderLayout.NORTH);
		newJPanel.add(knapper, BorderLayout.SOUTH);
		GUIObjekt.setContentPane(newJPanel);
		GUIObjekt.invalidate();
		GUIObjekt.validate();
	}

	/**
	 * Lagrer Issue objektene fra issueList til filen new_issues.xml
	 */
	private void lagreIssuesTilFil() {
		try {
			PrintWriter writer = new PrintWriter("new_issues.xml", "UTF-8");
			writer.println("<dataset>");
			for (Issue o : issueList) {
				writer.println("<ISSUES id=\"" + o.getId() + "\" assigned_user=\"" + o.getUser() + "\" created = \""
						+ o.getCreated() + "\" text =\"" + o.getText() + "\" priority=\"" + o.getPriority()
						+ "\" status=\"" + o.getStatus() + "\" opprettetAv=\"" + o.getOpprettetAv() + "\" endretAv=\""
						+ o.getEndretAv() + "\" location=\"" + o.getLocation() + "\"/>");
			}
			writer.println("</dataset>");
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Lagrer Bruker objektene fra users til brukere.xml
	 */
	private void lagreBrukerTilFil() {
		try {
			PrintWriter writer = new PrintWriter("brukere.xml", "UTF-8");
			writer.println("<dataset>");
			for (Bruker o : users) {
				writer.println("<USER brukernavn=\"" + o.getBrukernavn() + "\" email= \"" + o.getEmail()
						+ "\" fultNavn = \"" + o.getFultNavn() + "\" ansettelsesdato =\"" + o.getAnsettelsesDato()
						+ "\" passord = \"" + o.getPassord() + "\"/>");
			}
			writer.println("</dataset>");
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Viser kun Issues med datoer som er etter datoen den f�r inn. Ved feil
	 * syntax p� inn-datoen gj�r den ingenting
	 * 
	 * @param dato
	 *            en dato formatert mm/dd/yyyy
	 */
	private void sokDato(String dato) {
		ArrayList<Issue> tempList = findUsersIssues(readXML(), currentUsername);
		ArrayList<Issue> storageList = new ArrayList<Issue>();
		String datoliste[] = dato.split("-");
		Date forsteDato = createDate(datoliste[0]);
		Date andreDato = createDate(datoliste[1]);

		for (Issue o : tempList) {

			if (createDate(o.getCreated()).after(forsteDato) && createDate(o.getCreated()).before(andreDato))
				storageList.add(o);

		}
		table = returnIssueTable(storageList);
		GUIObjekt.remove(scrollPane);
		scrollPane = new JScrollPane(table);
		GUIObjekt.add(scrollPane, BorderLayout.NORTH);
		GUIObjekt.invalidate();
		GUIObjekt.validate();
		scrollPane.setVisible(true);
	}

	/**
	 * Tar inn en string og gj�r det om til et Date objekt
	 * 
	 * @param dato
	 *            en string representasjon av en dato
	 * @return Date representasjon av stringen
	 */
	private Date createDate(String dato) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String datoElementer[] = dato.split("/");
		// TO DO date format exception
		try {
			if (datoElementer[0].length() != 2)
				datoElementer[0] = "0" + datoElementer[0];
			if (datoElementer[1].length() == 2) {
				return sdf.parse(datoElementer[0] + "/" + datoElementer[1] + "/" + datoElementer[2]);
			} else {
				datoElementer[1] = "0" + datoElementer[1];
				return sdf.parse(datoElementer[0] + "/" + datoElementer[1] + "/" + datoElementer[2]);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * viser kun Issues med prioritering som er over eller lik prioriteringen
	 * den f�r inn.
	 * 
	 * @param prioritering
	 *            en String som gir prioritering
	 */
	private void sokPrioritering(String prioritering) {
		showList(prioriterSort(prioritering));
	}

	public ArrayList<Issue> prioriterSort(String prioritering) {
		ArrayList<Issue> tempList = findUsersIssues(readXML(), currentUsername);
		ArrayList<Issue> storageList = new ArrayList<Issue>();
		ArrayList<Issue> ikkePrioritert = new ArrayList<Issue>();
		ArrayList<Issue> normal = new ArrayList<Issue>();
		ArrayList<Issue> hoy = new ArrayList<Issue>();
		ArrayList<Issue> kritisk = new ArrayList<Issue>();
		for (Issue o : tempList) {
			switch (o.getPriority()) {
			case "ikke prioritert":
				ikkePrioritert.add(o);
				break;
			case "normal":
				normal.add(o);
				break;
			case "h�y":
				hoy.add(o);
				break;
			case "kritisk":
				kritisk.add(o);
				break;
			}
			// if (o.getPriority() >= prioritering)
			// storageList.add(o);
		}
		switch (prioritering) {
		case "ikke prioritert":
			storageList.addAll(kritisk);
			storageList.addAll(hoy);
			storageList.addAll(normal);
			storageList.addAll(ikkePrioritert);
			break;
		case "normal":
			storageList.addAll(kritisk);
			storageList.addAll(hoy);
			storageList.addAll(normal);
			break;
		case "h�y":
			storageList.addAll(kritisk);
			storageList.addAll(hoy);
			break;
		case "kritisk":
			storageList.addAll(hoy);
			break;
		}
		return storageList;
	}

	/**
	 * Initialiserer og viser et vindu for utfylling av informasjon om, og
	 * opprettelse av en bruker
	 */
	public void nyBrukerVindu() {
		brukernavnLabel = new JLabel("Brukernavn");
		brukernavnField = new JTextField();
		emailLabel = new JLabel("email");
		emailField = new JTextField();
		fultNavnLabel = new JLabel("Fult navn");
		fultNavnField = new JTextField();
		ansettelsesDatoLabel = new JLabel("Ansettelses dato");
		ansettelsesDatoField = new JTextField();
		passordLabel = new JLabel("Passord");
		passordField = new JTextField();
		nyAnsattButton = new Button("Ny ansatt");
		Button nyAnsatt = new Button("Ny ansatt");

		// ansettelsesDatoField.setPreferredSize(new Dimension(300, 300));

		JPanel vindu = new JPanel();
		vindu.setLayout(new GridLayout(0, 1));
		vindu.add(brukernavnLabel);
		vindu.add(brukernavnField);
		vindu.add(emailLabel);
		vindu.add(emailField);
		vindu.add(fultNavnLabel);
		vindu.add(fultNavnField);
		vindu.add(ansettelsesDatoLabel);
		vindu.add(ansettelsesDatoField);
		vindu.add(passordLabel);
		vindu.add(passordField);
		vindu.add(nyAnsattButton);
		vindu.add(avbrytButton);
		GUIObjekt.setContentPane(vindu);
		GUIObjekt.invalidate();
		GUIObjekt.validate();

		nyAnsattButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				users.add(new Bruker(brukernavnField.getText(), emailField.getText(), fultNavnField.getText(),
						ansettelsesDatoField.getText(), passordField.getText()));
				lagreBrukerTilFil();
				gaTilHovedVindu();
			}
		});

	}

	/**
	 * �pner hovedvinduet og viser listen;
	 */
	private void gaTilHovedVindu() {
		JPanel newJPanel = new JPanel();
		newJPanel.setLayout(new BorderLayout());
		newJPanel.add(scrollPane, BorderLayout.NORTH);
		newJPanel.add(redigeringsKnapper, BorderLayout.WEST);
		newJPanel.add(sok, BorderLayout.EAST);
		GUIObjekt.setContentPane(newJPanel);
		GUIObjekt.invalidate();
		GUIObjekt.validate();
	}

	/**
	 * Metoden tar inn en liste, og returnerer den sortert p� dato. For � t�le
	 * store mengder data s� brukes det radix sort med effektivitet p� litt
	 * d�rligere enn O(nk)
	 * 
	 * @param liste
	 *            en liste med objekter som skal sorteres
	 * @return ArrayList sortert p� dato
	 */
	public ArrayList<Issue> sortDato(ArrayList<Issue> liste) {
		class DatoNode {
			Issue issuePointer;
			int dateRep;

			public DatoNode(Issue issuePointer, int dateRep) {
				this.issuePointer = issuePointer;
				this.dateRep = dateRep;
			}

			public int getDateRep() {
				return dateRep;
			}

			public Issue getissuePointer() {
				return issuePointer;
			}

		}
		DatoNode[] datoListe = new DatoNode[liste.size()];
		for (int i = 0; i < liste.size(); i++) {
			datoListe[i] = new DatoNode(liste.get(i), gjorSammenlignbar(liste.get(i).getCreated()));
		}

		int i, m = datoListe[0].getDateRep(), exp = 1, n = datoListe.length;
		DatoNode[] b = new DatoNode[liste.size()];
		for (i = 1; i < n; i++)
			if (datoListe[i].getDateRep() > m)
				m = datoListe[i].getDateRep();
		while (m / exp > 0) {
			int[] bucket = new int[10];

			for (i = 0; i < n; i++)
				bucket[(datoListe[i].getDateRep() / exp) % 10]++;
			for (i = 1; i < 10; i++)
				bucket[i] += bucket[i - 1];
			for (i = n - 1; i >= 0; i--)
				b[--bucket[(datoListe[i].getDateRep() / exp) % 10]] = datoListe[i];
			for (i = 0; i < n; i++)
				datoListe[i] = b[i];
			exp *= 10;
		}
		ArrayList<Issue> newList = new ArrayList<Issue>();
		for (DatoNode dn : datoListe) {
			newList.add(dn.getissuePointer());
		}
		return newList;
	}

	/**
	 * Gj�r om en string til en int som man kan bruke radix sort p� ved � fylle
	 * inn nullere og omplassere elementene
	 * 
	 * @param dato
	 *            en String represrntasjon av en dato
	 * @return datoen listet med fremst �r, s� m�ned, s� dag.
	 */
	private int gjorSammenlignbar(String dato) {
		String datoElementer[] = dato.split("/");
		// TO DO date format exception
		if (datoElementer[0].length() != 2)
			datoElementer[0] = "0" + datoElementer[0];
		if (datoElementer[1].length() != 2) {
			datoElementer[1] = "0" + datoElementer[1];
		}
		return Integer.parseInt(datoElementer[2] + datoElementer[0] + datoElementer[1]);
	}

	/**
	 * Sorterer en liste utifra om elementene er l�st eller ikke med ul�ste
	 * elementer f�rst
	 * 
	 * @param liste
	 *            liste med Issue objekter � sortere
	 * @return returnerer en liste med Issue objekter sortert f�rst ul�st, s�
	 *         l�st
	 */
	public ArrayList<Issue> sortLost(ArrayList<Issue> liste) {
		ArrayList<Issue> lostList = new ArrayList<Issue>();
		ArrayList<Issue> ulostList = new ArrayList<Issue>();
		for (Issue i : liste) {
			if (i.getStatus().equals("l�st")) {
				lostList.add(i);
			} else
				ulostList.add(i);
		}
		ulostList.addAll(lostList);
		return ulostList;
	}
}
