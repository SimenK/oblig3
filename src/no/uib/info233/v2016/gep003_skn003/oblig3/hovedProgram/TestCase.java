package no.uib.info233.v2016.gep003_skn003.oblig3.hovedProgram;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class TestCase {
	
	/**
	 * lager logic objekt
	 */
	GUI test;
	Logic logicObjekt;
	ArrayList<Issue> liste;
	
	
	 @Before
	 public void init(){
		 test = new GUI();
			test.lagOgVisGUI();
			logicObjekt = new Logic(test);
			logicObjekt.readXML();
			liste = logicObjekt.getIssueList();
	 }

	/**
	 * Sjekker at ingen elementer har blitt fjernet fra xml filen ved � sammenligne antallet filer og maximum id
	 */
	@Test
	public void xmlReadingTest() {
	
		assertTrue(logicObjekt.getIssueList().size() == logicObjekt.getMaxId());
	}
	
	
	/**
	 * sjekker om det blir riktig sortert utifra dato 
	 */
	

	@Test
	public void datoSortTest() {
		
		liste = logicObjekt.sortDato(liste);
		for(Issue i: liste)
		System.out.println(i.getCreated());
		
	}
	
	/**
	 * sjekker om det blir sortert utifra om det er l�st
	 */
	@Test
	public void lostSortTest() {
		
		liste = logicObjekt.sortLost(liste);
		for(Issue i : liste)
		System.out.println(i.getStatus());
		
	}
	

}

