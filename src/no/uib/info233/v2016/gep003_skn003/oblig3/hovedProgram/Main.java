package no.uib.info233.v2016.gep003_skn003.oblig3.hovedProgram;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Main {

	/**
	 * initialiserer programmet
	 * @param args er tom
	 */
	public static void main(String[] args) {
		GUI test = new GUI();
		test.lagOgVisGUI();
		Logic logicObjekt = new Logic(test);
		//logicObjekt.editIssues();
	}

}
