package no.uib.info233.v2016.gep003_skn003.oblig3.hovedProgram;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class GUI extends JFrame {
	// Logic logicObject = new Logic();
	JTextField sokefelt;
	JTextField passordField;
	JTextField brukernavnField;
	Button loggInnButton;
	JLabel brukernavnLabel;
	JLabel passordLabel;
	JLabel tom;
	JPanel frame;

	/**
	 * returnerer frame
	 * @return frame
	 */
	public JPanel getFrame() {
		return frame;
	}

	/**
	 * setterFrame
	 * @param frame
	 */
	public void setFrame(JPanel frame) {
		this.frame = frame;
	}

	/**
	 * returnerer passordField
	 * @return passordField
	 */
	public JTextField getPassordField() {
		return passordField;
	}

	/**
	 * setter passord
	 * @param passordField
	 */
	public void setPassordField(JTextField passordInput) {
		this.passordField = passordInput;
	}

	/**
	 * returnerer brukernavnField
	 * @return brukernavnField
	 */
	public JTextField getBrukernavnField() {
		return brukernavnField;
	}

	/**
	 * setter brukernavnField
	 * @param brukernavnField
	 */
	public void setBrukernavnField(JTextField brukernavnField) {
		this.brukernavnField = brukernavnField;
	}

	/**
	 * returnerer loggInnButton
	 * @return loggInnButton
	 */
	public Button getLoggInnButton() {
		return loggInnButton;
	}

	/**
	 * setter loggInnButton
	 * @param loggInnButton
	 */
	public void setLoggInnButton(Button loggInnButton) {
		this.loggInnButton = loggInnButton;
	}

	/**
	 * konstukt�r for GUI objekt
	 */
	GUI() {

	}

	/**
	 * lager og oppretter GUI
	 */
	public void lagOgVisGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addComponents(getContentPane());
		setSize(1200, 500);
		setVisible(true);
	}

	/**
	 * legger til de f�rste elementene i GUI
	 * @param pane tar inn pane
	 */
	private void addComponents(final Container pane) {
		frame = new JPanel();
		frame.setLayout(new GridLayout(4, 0));
		brukernavnField = new JTextField("dryan3");
		passordField = new JTextField("123");
		loggInnButton = new Button("Logg inn");
		brukernavnLabel = new JLabel("Brukernavn:");
		passordLabel = new JLabel("Passord:");
		tom = new JLabel("Passord er 123");

		frame.add(brukernavnLabel);
		frame.add(brukernavnField);
		frame.add(passordLabel);
		frame.add(passordField);
		frame.add(tom);
		frame.add(loggInnButton);
		pane.setSize(300, 200);
		pane.add(frame);

	}

}
