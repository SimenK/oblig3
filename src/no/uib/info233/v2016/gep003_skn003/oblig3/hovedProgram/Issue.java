package no.uib.info233.v2016.gep003_skn003.oblig3.hovedProgram;

public class Issue {

	private int id;
	private String user;
	private String created;
	private String text;
	private String priority;
	private String location;
	private boolean lost;
	private String endretAv;
	private String status;
	private String opprettetAv;

	public String getOpprettetAv() {
		
		return opprettetAv;
	}

	public void setOpprettetAv(String opprettetAv) {
		this.opprettetAv = opprettetAv;
	}

	public boolean isLost() {
		return lost;
	}

	public void setLost(boolean lost) {
		this.lost = lost;
	}

	public String getEndretAv() {
		return endretAv;
	}

	public void setEndretAv(String endretAv) {
		this.endretAv = endretAv;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returnerer id
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * setter id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * returnerer user
	 * @return user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * setter user
	 * @param user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * returnerer created
	 * @return created
	 */
	public String getCreated() {
		return created;
	}

	/**
	 * setter created
	 * @param created
	 */
	public void setCreated(String created) {
		this.created = created;
	}

	/**
	 * returnerer text
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * setter text
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * returnerer priority
	 * @return priority
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * setter priority
	 * @param priority
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * returnerer location
	 * @return location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * setter location
	 * @param location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * konstruktÝr for Issue
	 */
	public Issue(int id, String user, String created, String text, String priority, String status, String opprettetAv, String endretAv, String location) {
		super();
		this.id = id;
		this.user = user;
		this.created = created;
		this.text = text;
		this.priority = priority;
		this.location = location;
		this.endretAv = endretAv;
		this.status = status;
		this.opprettetAv = opprettetAv;
	}

	@Override
	public String toString() {
		return "Issue [id=" + id + ", user=" + user + ", created=" + created + ", text=" + text + ", priority="
				+ priority + ", location=" + location + ", lost=" + lost + ", endretAv=" + endretAv + ", status="
				+ status + "]";
	}

	

}
