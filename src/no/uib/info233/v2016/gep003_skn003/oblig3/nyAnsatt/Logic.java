package no.uib.info233.v2016.gep003_skn003.oblig3.nyAnsatt;

import java.awt.Button;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import no.uib.info233.v2016.gep003_skn003.oblig3.Bruker;
import no.uib.info233.v2016.gep003_skn003.oblig3.hovedProgram.Issue;

public class Logic {
	ArrayList<Issue> issueList;
	ArrayList<Bruker> users;

	GUI GUIObjekt;
	String currentUsername;
	String currentLoggedIn;

	JPanel editor;
	JLabel idLabel;
	JLabel userLabel;
	JLabel createdLabel;
	JTextArea textArea;
	JTextField priorityField;
	JTextField locationField;
	JComboBox userList;
	JComboBox priorityList;
	Button lagreButton;
	Button avbrytButton = new Button("Avbryt");

	int maxId = 0;

	Logic(GUI test) {
		readXML();
		GUIObjekt = test;
		GUIObjekt.getLoggInnButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean userFound = false;
				int i = 0;
				while (i < users.size() && !userFound) {
					if (users.get(i).getBrukernavn().equals(GUIObjekt.getBrukernavnField().getText()))
						userFound = true;
				}
				if (userFound && users.get(i).getPassord().equals(GUIObjekt.getPassordField().getText())) {
					currentUsername = GUIObjekt.getBrukernavnField().getText();
					currentLoggedIn = GUIObjekt.getBrukernavnField().getText();
					System.out.println(currentUsername);
					editIssue();

					GUIObjekt.getFrame().setVisible(false);
					avbrytButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							// gaTilHovedVindu();
						}
					});
				}
			}
		});

	}

	public ArrayList<Issue> readXML() {
		issueList = new ArrayList<Issue>();
		users = new ArrayList<Bruker>();
		try {
			String linje;

			BufferedReader rdr = new BufferedReader(
					new InputStreamReader(new FileInputStream("new_issues.xml"), "UTF8"));
			rdr.readLine();
			while ((linje = rdr.readLine()) != null) {
				String liste[] = linje.split("\"");
				if (liste.length > 5 && liste[0] != null) {
					issueList.add(new Issue(Integer.parseInt(liste[1]), liste[3], liste[5], liste[7], liste[9],
							liste[11], liste[13], liste[15], liste[17]));
					if (Integer.parseInt(liste[1]) > maxId)
						maxId = Integer.parseInt(liste[1]);
				}
			}
			rdr.close();
			rdr = new BufferedReader(new FileReader("brukere.xml"));
			rdr.readLine();
			while ((linje = rdr.readLine()) != null) {
				String liste[] = linje.split("\"");
				if (liste.length > 4 && liste[0] != null) {
					users.add(new Bruker(liste[1], liste[3], liste[5], liste[7], liste[9]));

				}
			}

			return issueList;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Alt gikk galt");
			return null;
		}
	}

	private void editIssue() {
		editor = new JPanel();
		textArea = new JTextArea();
		priorityField = new JTextField();
		locationField = new JTextField();
		JLabel priorityLabel = new JLabel("Priority: ");
		JLabel locationLabel = new JLabel("Location: ");
		lagreButton = new Button("Lagre");
		int newId = issueList.get(issueList.size() - 1).getId() + 1;
		idLabel = new JLabel("ID: " + newId);
		editor.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		String[] priorityArray = new String[] { "Ikke prioritert", "Normal", "H�y", "Kritisk" };
		priorityList = new JComboBox(priorityArray);

		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;

		String[] usersArray = new String[users.size()];
		for (int i = 0; i < users.size(); i++)
			usersArray[i] = users.get(i).getBrukernavn();
		userList = new JComboBox(usersArray);
		editor.add(userList, c);

		String newCreated;
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int year = localDate.getYear();
		int month = localDate.getMonthValue();
		int day = localDate.getDayOfMonth();
		newCreated = "" + month + "/" + day + "/" + year;
		c.gridx = 1;
		c.gridy = 0;
		editor.add(createdLabel = new JLabel("Created: " + newCreated), c);

		c.gridx = 2;
		c.gridy = 0;
		editor.add(idLabel, c);
		JCheckBox solved = new JCheckBox("L�st");
		// editor.add(userLabel);
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 4;
		editor.add(textArea, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 2;
		editor.add(priorityLabel, c);
		c.gridx = 1;
		c.gridy = 2;
		editor.add(priorityList, c);
		c.gridx = 2;
		c.gridy = 2;
		editor.add(locationLabel, c);
		c.gridx = 3;
		c.gridy = 2;
		editor.add(locationField, c);
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 2;
		editor.add(lagreButton, c);
		c.gridx = 2;
		c.gridy = 3;
		editor.add(avbrytButton, c);
		GUIObjekt.setContentPane(editor);
		// table.setVisible(false);
		GUIObjekt.invalidate();
		GUIObjekt.validate();
		editor.setVisible(true);

		lagreButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textArea.getText().equals("") && !locationField.getText().equals("")) {

					issueList.add(new Issue(newId, (String) userList.getSelectedItem(), newCreated, textArea.getText(),
							(String) priorityList.getSelectedItem(), "Ul�st", currentLoggedIn, "",
							locationField.getText()));
					lagreIssuesTilFil();
				} else {
					JOptionPane.showMessageDialog(null, "Du m� fylle ut tekst og location",
							"Du m� fylle ut tekst og location", JOptionPane.ERROR_MESSAGE);
				}

				// gaTilHovedVindu();
			}
		});

	}

	/**
	 * Lagrer Issue objektene fra issueList til filen new_issues.xml
	 */
	private void lagreIssuesTilFil() {
		try {
			PrintWriter writer = new PrintWriter("new_issues.xml", "UTF-8");
			writer.println("<dataset>");
			for (Issue o : issueList) {
				writer.println("<ISSUES id=\"" + o.getId() + "\" assigned_user=\"" + o.getUser() + "\" created = \""
						+ o.getCreated() + "\" text =\"" + o.getText() + "\" priority=\"" + o.getPriority()
						+ "\" status=\"" + o.getStatus() + "\" opprettetAv=\"" + o.getOpprettetAv() + "\" endretAv=\""
						+ "\" location=\"" + o.getLocation() + "\"/>");
			}
			writer.println("</dataset>");
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
